# as build means the temporary build image to use it later for nginx
FROM node:18-alpine as build

WORKDIR /app
COPY package.json .
COPY package-lock.json .

RUN npm install

COPY src/ .
COPY public/ .

RUN npm run build

FROM nginx:stable-alpine

COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
