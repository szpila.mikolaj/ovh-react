import React, { useState } from 'react';
import './App.css';
import { localSettings } from './common/LocalSettings';
import { Button, ButtonType } from './components/atom/button/Button';
import H1 from './components/atom/h1/H1';
import { LoginUseCase } from './useCases/auth/LoginUseCase';
import { FetchNonRestrictedResource } from './useCases/core-domain/FetchNonRestrictedResource';
import { FetchRestrictedResource } from './useCases/core-domain/FetchRestrictedResource';

function App() {
  const [fetchedData, setFetchedData] = useState<string>('No data fetched');
  const [fetchedToken, setFetchedToken] = useState<string>('No token fetched');

  const login = async (): Promise<void> => {
    if (localSettings.getValue('token')) return;

    const token = await new LoginUseCase().exec();
    localSettings.setValue('token', token.value);
    setFetchedToken(token.value);
  };

  const fetchNonRestrictedResource = async (): Promise<void> => {
    const data = await new FetchNonRestrictedResource().exec();
    setFetchedData(`Data from non restricted API: Resource ID: ${ data.id }, Restricted: ${ data.restrictedAccess }`);
  };

  const fetchRestrictedResource = async (): Promise<void> => {
    const data = await new FetchRestrictedResource().exec();
    setFetchedData(`Data from restricted API: Resource ID: ${ data.id }, Restricted: ${ data.restrictedAccess }`);
  };

  return (
    <div className="App">
      <H1>Token: { fetchedToken ? fetchedToken : 'not fetched' }</H1>
      <Button content={ 'Login' } onClick={ login } type={ ButtonType.SECONDARY }></Button>
      <H1>Data: { fetchedData ? fetchedData : 'not fetched' }</H1>
      <Button content={ 'Fetch non restricted resource' } onClick={ fetchNonRestrictedResource } type={ ButtonType.QUATERNARY }></Button>
      <Button content={ 'Fetch restricted resource' } onClick={ fetchRestrictedResource } type={ ButtonType.RED }></Button>
    </div>
  );
}

export default App;
