import { UseCase } from '../../common/UseCase';
import { API } from '../../common/api/API';

export interface AuthorizationResult {
  value: string;
}

export class LoginUseCase extends UseCase<{}, AuthorizationResult> {
  protected async execute() {
    return API.HTTP.post<AuthorizationResult>('iam/login');
  }
}