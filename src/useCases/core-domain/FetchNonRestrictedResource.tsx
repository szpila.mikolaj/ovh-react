import { UseCase } from '../../common/UseCase';
import { API } from '../../common/api/API';

export interface AuthorizationResult {
  id: string;
  restrictedAccess: boolean;
}

export class FetchNonRestrictedResource extends UseCase<{}, AuthorizationResult> {
  protected async execute() {
    return API.HTTP.get<AuthorizationResult>('resource/non-restricted');
  }
}