import { UseCase } from '../../common/UseCase';
import { API } from '../../common/api/API';

export interface AuthorizationResult {
  id: string;
  restrictedAccess: boolean;
}

export class FetchRestrictedResource extends UseCase<{}, AuthorizationResult> {
  protected async execute() {
    return API.HTTP.get<AuthorizationResult>('resource/restricted');
  }
}